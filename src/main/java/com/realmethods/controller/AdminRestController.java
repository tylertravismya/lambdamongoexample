/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements Struts action processing for business entity Admin.
 *
 * @author Dev Team
 */
@RestController
@RequestMapping("/Admin")
public class AdminRestController extends BaseSpringRestController
{

    /**
     * Handles saving a Admin BO.  if not key provided, calls create, otherwise calls save
     * @param		Admin admin
     * @return		Admin
     */
	@RequestMapping("/save")
    public Admin save( @RequestBody Admin admin )
    {
    	// assign locally
    	this.admin = admin;
    	
        if ( hasPrimaryKey() )
        {
            return (update());
        }
        else
        {
            return (create());
        }
    }

    /**
     * Handles deleting a Admin BO
     * @param		Long adminId
     * @param 		Long[] childIds
     * @return		boolean
     */
    @RequestMapping("/delete")    
    public boolean delete( @RequestParam(value="admin.adminId", required=false) Long adminId, 
    						@RequestParam(value="childIds", required=false) Long[] childIds )
    {                
        try
        {
        	AdminBusinessDelegate delegate = AdminBusinessDelegate.getAdminInstance();
        	
        	if ( childIds == null || childIds.length == 0 )
        	{
        		delegate.delete( new AdminPrimaryKey( adminId ) );
        		LOGGER.info( "AdminController:delete() - successfully deleted Admin with key " + admin.getAdminPrimaryKey().valuesAsCollection() );
        	}
        	else
        	{
        		for ( Long id : childIds )
        		{
        			try
        			{
        				delegate.delete( new AdminPrimaryKey( id ) );
        			}
	                catch( Throwable exc )
	                {
	                	LOGGER.info( "AdminController:delete() - " + exc.getMessage() );
	                	return false;
	                }
        		}
        	}
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AdminController:delete() - " + exc.getMessage() );
        	return false;        	
        }
        
        return true;
	}        
	
    /**
     * Handles loading a Admin BO
     * @param		Long adminId
     * @return		Admin
     */    
    @RequestMapping("/load")
    public Admin load( @RequestParam(value="admin.adminId", required=true) Long adminId )
    {    	
        AdminPrimaryKey pk = null;

    	try
        {  
    		System.out.println( "\n\n****Admin.load pk is " + adminId );
        	if ( adminId != null )
        	{
        		pk = new AdminPrimaryKey( adminId );
        		
        		// load the Admin
	            this.admin = AdminBusinessDelegate.getAdminInstance().getAdmin( pk );
	            
	            LOGGER.info( "AdminController:load() - successfully loaded - " + this.admin.toString() );             
			}
			else
			{
	            LOGGER.info( "AdminController:load() - unable to locate the primary key as an attribute or a selection for - " + admin.toString() );
	            return null;
			}	            
        }
        catch( Throwable exc )
        {
            LOGGER.info( "AdminController:load() - failed to load Admin using Id " + adminId + ", " + exc.getMessage() );
            return null;
        }

        return admin;

    }

    /**
     * Handles loading all Admin business objects
     * @return		List<Admin>
     */
    @RequestMapping("/loadAll")
    public List<Admin> loadAll()
    {                
        List<Admin> adminList = null;
        
    	try
        {                        
            // load the Admin
            adminList = AdminBusinessDelegate.getAdminInstance().getAllAdmin();
            
            if ( adminList != null )
                LOGGER.info(  "AdminController:loadAllAdmin() - successfully loaded all Admins" );
        }
        catch( Throwable exc )
        {
            LOGGER.info(  "AdminController:loadAll() - failed to load all Admins - " + exc.getMessage() );
        	return null;
            
        }

        return adminList;
                            
    }


// findAllBy methods



    /**
     * save Users on Admin
     * @param		Long adminId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		Admin
     */     
	@RequestMapping("/saveUsers")
	public Admin saveUsers( @RequestParam(value="admin.adminId", required=false) Long adminId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( adminId ) == null )
			return( null );
		
		UserPrimaryKey pk 					= null;
		User child							= null;
		UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
		AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new UserPrimaryKey( childId );
			
			try
			{
				// find the User
				child = childDelegate.getUser( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AdminController:saveUsers() failed get child User using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the Users 
			admin.getUsers().add( child );				
		}
		else
		{
			// clear out the Users but 
			admin.getUsers().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new UserPrimaryKey( id );
					try
					{
						// find the User
						child = childDelegate.getUser( pk );
						// add it to the Users List
						admin.getUsers().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "AdminController:saveUsers() failed get child User using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the Admin
			parentDelegate.saveAdmin( admin );
		}
		catch( Exception exc )
		{
			LOGGER.info( "AdminController:saveUsers() failed saving parent Admin - " + exc.getMessage() );
		}

		return admin;
	}

    /**
     * delete Users on Admin
     * @param		Long adminId
     * @param		Long[] childIds
     * @return		Admin
     */     	
	@RequestMapping("/deleteUsers")
	public Admin deleteUsers( @RequestParam(value="admin.adminId", required=true) Long adminId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( adminId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			UserPrimaryKey pk 					= null;
			UserBusinessDelegate childDelegate 	= UserBusinessDelegate.getUserInstance();
			AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();
			Set<User> children					= admin.getUsers();
			User child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new UserPrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getUser( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "AdminController:deleteUsers() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of User back to the admin
			admin.setUsers( children );			
			// save it 
			try
			{
				admin = parentDelegate.saveAdmin( admin );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "AdminController:deleteUsers() failed to save the Admin - " + exc.getMessage() );
			}
		}
		
		return admin;
	}

    /**
     * save ReferenceEngines on Admin
     * @param		Long adminId
     * @param		Long childId
     * @param		Long[] childIds
     * @return		Admin
     */     
	@RequestMapping("/saveReferenceEngines")
	public Admin saveReferenceEngines( @RequestParam(value="admin.adminId", required=false) Long adminId, 
											@RequestParam(value="childIds", required=false) Long childId, @RequestParam("") Long[] childIds )
	{
		if ( load( adminId ) == null )
			return( null );
		
		ReferenceEnginePrimaryKey pk 					= null;
		ReferenceEngine child							= null;
		ReferenceEngineBusinessDelegate childDelegate 	= ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
		AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();		
		
		if ( childId != null || childIds.length == 0 )// creating or saving one
		{
			pk = new ReferenceEnginePrimaryKey( childId );
			
			try
			{
				// find the ReferenceEngine
				child = childDelegate.getReferenceEngine( pk );
			}
			catch( Exception exc )
			{
				LOGGER.info( "AdminController:saveReferenceEngines() failed get child ReferenceEngine using id " + childId  + "- " + exc.getMessage() );
				return( null );
			}
			
			// add it to the ReferenceEngines 
			admin.getReferenceEngines().add( child );				
		}
		else
		{
			// clear out the ReferenceEngines but 
			admin.getReferenceEngines().clear();
			
			// finally, find each child and add it
			if ( childIds != null )
			{
				for( Long id : childIds )
				{
					pk = new ReferenceEnginePrimaryKey( id );
					try
					{
						// find the ReferenceEngine
						child = childDelegate.getReferenceEngine( pk );
						// add it to the ReferenceEngines List
						admin.getReferenceEngines().add( child );
					}
					catch( Exception exc )
					{
						LOGGER.info( "AdminController:saveReferenceEngines() failed get child ReferenceEngine using id " + id  + "- " + exc.getMessage() );
					}
				}
			}
		}

		try
		{
			// save the Admin
			parentDelegate.saveAdmin( admin );
		}
		catch( Exception exc )
		{
			LOGGER.info( "AdminController:saveReferenceEngines() failed saving parent Admin - " + exc.getMessage() );
		}

		return admin;
	}

    /**
     * delete ReferenceEngines on Admin
     * @param		Long adminId
     * @param		Long[] childIds
     * @return		Admin
     */     	
	@RequestMapping("/deleteReferenceEngines")
	public Admin deleteReferenceEngines( @RequestParam(value="admin.adminId", required=true) Long adminId, 
											@RequestParam(value="childIds", required=false) Long[] childIds )
	{		
		if ( load( adminId ) == null )
			return( null );

		if ( childIds != null || childIds.length == 0 )
		{
			ReferenceEnginePrimaryKey pk 					= null;
			ReferenceEngineBusinessDelegate childDelegate 	= ReferenceEngineBusinessDelegate.getReferenceEngineInstance();
			AdminBusinessDelegate parentDelegate = AdminBusinessDelegate.getAdminInstance();
			Set<ReferenceEngine> children					= admin.getReferenceEngines();
			ReferenceEngine child 							= null;
			
			for( Long id : childIds )
			{
				try
				{
					pk = new ReferenceEnginePrimaryKey( id );
					
					// first remove the relevant child from the list
					child = childDelegate.getReferenceEngine( pk );
					children.remove( child );
					
					// then safe to delete the child				
					childDelegate.delete( pk );
				}
				catch( Exception exc )
				{
					LOGGER.info( "AdminController:deleteReferenceEngines() failed - " + exc.getMessage() );
				}
			}
			
			// assign the modified list of ReferenceEngine back to the admin
			admin.setReferenceEngines( children );			
			// save it 
			try
			{
				admin = parentDelegate.saveAdmin( admin );
			}
			catch( Throwable exc )
			{
				LOGGER.info( "AdminController:deleteReferenceEngines() failed to save the Admin - " + exc.getMessage() );
			}
		}
		
		return admin;
	}


    /**
     * Handles creating a Admin BO
     * @return		Admin
     */
    protected Admin create()
    {
        try
        {       
			this.admin = AdminBusinessDelegate.getAdminInstance().createAdmin( admin );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AdminController:create() - exception Admin - " + exc.getMessage());        	
        	return null;
        }
        
        return this.admin;
    }

    /**
     * Handles updating a Admin BO
     * @return		Admin
     */    
    protected Admin update()
    {
    	// store provided data
        Admin tmp = admin;

        // load actual data from db
    	load();
    	
    	// copy provided data into actual data
    	admin.copyShallow( tmp );
    	
        try
        {                        	        
			// create the AdminBusiness Delegate            
			AdminBusinessDelegate delegate = AdminBusinessDelegate.getAdminInstance();
            this.admin = delegate.saveAdmin( admin );
            
            if ( this.admin != null )
                LOGGER.info( "AdminController:update() - successfully updated Admin - " + admin.toString() );
        }
        catch( Throwable exc )
        {
        	LOGGER.info( "AdminController:update() - successfully update Admin - " + exc.getMessage());        	
        	return null;
        }
        
        return this.admin;
        
    }


    /**
     * Returns true if the admin is non-null and has it's primary key field(s) set
     * @return		boolean
     */
    protected boolean hasPrimaryKey()
    {
    	boolean hasPK = false;

		if ( admin != null && admin.getAdminPrimaryKey().hasBeenAssigned() == true )
		   hasPK = true;
		
		return( hasPK );
    }

    protected Admin load()
    {
    	return( load( new Long( admin.getAdminPrimaryKey().getFirstKey().toString() ) ));
    }

//************************************************************************    
// Attributes
//************************************************************************
    protected Admin admin = null;
    private static final Logger LOGGER = Logger.getLogger(Admin.class.getName());
    
}



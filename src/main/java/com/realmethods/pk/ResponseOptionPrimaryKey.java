/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.primarykey;

import java.util.*;

/**
 * ResponseOption PrimaryKey class.
 * 
 * @author    Dev Team
 */
// AIB : #getPrimaryKeyClassDecl() 
public class ResponseOptionPrimaryKey 

    extends BasePrimaryKey
{
// ~AIB

//************************************************************************
// Public Methods
//************************************************************************

    /** 
     * default constructor - should be normally used for dynamic instantiation
     */
    public ResponseOptionPrimaryKey() 
    {
    }    
    
    /** 
     * Constructor with all arguments relating to the primary key
     * 	        
// AIB : #getAllAttributeJavaComments( true true )
    * @param    responseOptionId
// ~AIB     
     * @exception IllegalArgumentException
     */
    public ResponseOptionPrimaryKey(    
// AIB : #getAllPrimaryKeyArguments( $classObject false )
 		Object responseOptionId 			
// ~AIB
                         ) 
    throws IllegalArgumentException
    {
        super();
// AIB : #getKeyFieldAssignments()
		this.responseOptionId = responseOptionId != null ? new Long( responseOptionId.toString() ) : null;
// ~AIB 
    }   

    
//************************************************************************
// Access Methods
//************************************************************************

// AIB : #getKeyFieldAccessMethods()
   /**
	* Returns the responseOptionId.
	* @return    Long
    */    
	public Long getResponseOptionId()
	{
		return( this.responseOptionId );
	}            
	
   /**
	* Assigns the responseOptionId.
	* @return    Long
    */    
	public void setResponseOptionId( Long id )
	{
		this.responseOptionId = id;
	}            
	
// ~AIB 	         	     

    /**
     * Retrieves the value(s) as a single List
     * @return List
     */
    public List keys()
    {
		// assign the attributes to the Collection back to the parent
        ArrayList keys = new ArrayList();
        
		keys.add( responseOptionId );

        return( keys );
    }

	public Object getFirstKey()
	{
		return( responseOptionId );
	}

 
//************************************************************************
// Protected / Private Methods
//************************************************************************

    
//************************************************************************
// Attributes
//************************************************************************

 	

// DO NOT ASSIGN VALUES DIRECTLY TO THE FOLLOWING ATTRIBUTES.  SET THE VALUES
// WITHIN THE ResponseOption class.

// AIB : #getKeyFieldDeclarations()
	public Long responseOptionId;
// ~AIB 	        

}

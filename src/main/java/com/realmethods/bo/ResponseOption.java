/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.bo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

    import com.realmethods.primarykey.*;
    import com.realmethods.bo.*;
    
/**
 * Encapsulates data for business entity ResponseOption.
 * 
 * @author Dev Team
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity( value = "com.realmethods.bo.ResponseOption", noClassnameStored = true )
 public class ResponseOption extends Base
 {

//************************************************************************
// Constructors
//************************************************************************

    /** 
     * Default Constructor 
     */
    public ResponseOption() 
    {
    }   

//************************************************************************
// Accessor Methods
//************************************************************************

    /** 
     * Returns the ResponseOptionPrimaryKey
     * @return ResponseOptionPrimaryKey   
     */
    public ResponseOptionPrimaryKey getResponseOptionPrimaryKey() 
    {    
    	ResponseOptionPrimaryKey key = new ResponseOptionPrimaryKey(); 
		key.setResponseOptionId( this.responseOptionId );
        return( key );
    } 

    
// AIB : #getBOAccessorMethods(true)
   /**
	* Returns the responseText
  	* @return String	
	*/                    		    	    	    
	public String getResponseText() 	    	   
	{
		return this.responseText;		
	}
	
   /**
    * Assigns the responseText
    * @param responseText	String
    */
    public void setResponseText( String responseText )
    {
        this.responseText = responseText;
    }
    	
   /**
	* Returns the gotoQuestionId
  	* @return String	
	*/                    		    	    	    
	public String getGotoQuestionId() 	    	   
	{
		return this.gotoQuestionId;		
	}
	
   /**
    * Assigns the gotoQuestionId
    * @param gotoQuestionId	String
    */
    public void setGotoQuestionId( String gotoQuestionId )
    {
        this.gotoQuestionId = gotoQuestionId;
    }
    	
   /**
	* Returns the responseOptionId
  	* @return Long	
	*/                    		    	    	    
	public Long getResponseOptionId() 	    	   
	{
		return this.responseOptionId;		
	}
	
   /**
    * Assigns the responseOptionId
    * @param responseOptionId	Long
    */
    public void setResponseOptionId( Long responseOptionId )
    {
        this.responseOptionId = responseOptionId;
    }
    	
// ~AIB
 
    /**
     * Performs a shallow copy.
     * @param object 	ResponseOption		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copyShallow( ResponseOption object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ResponseOption:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( false )
        this.responseOptionId = object.getResponseOptionId();
        this.responseText = object.getResponseText();
        this.gotoQuestionId = object.getGotoQuestionId();
// ~AIB 

    }

    /**
     * Performs a deep copy.
     * @param object 	ResponseOption		copy source
     * @exception IllegalArgumentException 	Thrown if the passed in obj is null. It is also
     * 							thrown if the passed in businessObject is not of the correct type.
     */
    public void copy( ResponseOption object ) 
    throws IllegalArgumentException
    {
        if ( object == null )
        {
            throw new IllegalArgumentException(" ResponseOption:copy(..) - object cannot be null.");           
        }

        // Call base class copy
        super.copy( object );
        
        // Set member attributes
                
// AIB : #getCopyString( true )
// ~AIB 

    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString()
    {
        StringBuilder returnString = new StringBuilder();

        returnString.append( super.toString() + ", " );     

// AIB : #getToString( false )
		returnString.append( "responseOptionId = " + this.responseOptionId + ", ");
		returnString.append( "responseText = " + this.responseText + ", ");
		returnString.append( "gotoQuestionId = " + this.gotoQuestionId + ", ");
// ~AIB 

        return returnString.toString();
    }

	public java.util.Collection<String> attributesByNameUserIdentifiesBy()
	{
		Collection<String> names = new java.util.ArrayList<String>();
				
	return( names );
	}	
	
    public String getIdentity()
    {
		StringBuilder identity = new StringBuilder( "ResponseOption" );
		
			identity.append(  "::" );
		identity.append( responseOptionId );
	        return ( identity.toString() );
    }

    public String getObjectType()
    {
        return ("ResponseOption");
    }	

//************************************************************************
// Object Overloads
//************************************************************************

	public boolean equals( Object object )
	{
	    Object tmpObject = null;	    
	    if (this == object) 
	        return true;
	        
		if ( object == null )
			return false;
			
	    if (!(object instanceof ResponseOption)) 
	        return false;
	        
		ResponseOption bo = (ResponseOption)object;
		
		return( getResponseOptionPrimaryKey().equals( bo.getResponseOptionPrimaryKey() ) ); 
	}
	
	
// attributes

// AIB : #getAttributeDeclarations( true  )
@Id
 		protected Long responseOptionId = null;
      public String responseText = null;
      public String gotoQuestionId = null;
// ~AIB

}

/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test ReferenceEngine class.
 *
 * @author    Dev Team
 */
public class ReferenceEngineTest
{

// constructors

    public ReferenceEngineTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a ReferenceEngine, through a ReferenceEngineTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ReferenceEngineTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ReferenceEngineTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new ReferenceEngine.
     *
     * @return    ReferenceEngine
     */
    public ReferenceEngine testCreate()
    throws Throwable
    {
        ReferenceEngine businessObject = null;

    	{
	        LOGGER.info( "ReferenceEngineTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a ReferenceEngine");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a ReferenceEngine" );
	
	        try 
	        {            
	            businessObject = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().createReferenceEngine( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ReferenceEnginePrimaryKey)businessObject.getReferenceEnginePrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a ReferenceEngine with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a ReferenceEngine.
     *
     * @return    ReferenceEngine  
     */
    public ReferenceEngine testRead()
    throws Throwable
    {
        LOGGER.info( "ReferenceEngineTest:testRead()" );
        LOGGER.info( "-- Reading a previously created ReferenceEngine" );

        ReferenceEngine businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read ReferenceEngine with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().getReferenceEngine( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found ReferenceEngine " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a ReferenceEngine.
     *
     * @return    ReferenceEngine
     */
    public ReferenceEngine testUpdate()
    throws Throwable
    {

        LOGGER.info( "ReferenceEngineTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a ReferenceEngine." );

        StringBuilder msg = new StringBuilder( "Failed to update a ReferenceEngine : " );        
        ReferenceEngine businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created ReferenceEngine." );
            
            // for use later on...
            thePrimaryKey = (ReferenceEnginePrimaryKey)businessObject.getReferenceEnginePrimaryKey();
            
            ReferenceEngineBusinessDelegate proxy = ReferenceEngineBusinessDelegate.getReferenceEngineInstance();            
            businessObject = proxy.saveReferenceEngine( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved ReferenceEngine - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a ReferenceEngine.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ReferenceEngineTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created ReferenceEngine." );
        
        try
        {
            ReferenceEngineBusinessDelegate.getReferenceEngineInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted ReferenceEngine with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete ReferenceEngine with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all ReferenceEngines.
     *
     * @return    Collection
     */
    public ArrayList<ReferenceEngine> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ReferenceEngineTest:testGetAll() - Retrieving Collection of ReferenceEngines:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all ReferenceEngine : " );        
        ArrayList<ReferenceEngine> collection  = null;

        try
        {
            // call the static get method on the ReferenceEngineBusinessDelegate
            collection = ReferenceEngineBusinessDelegate.getReferenceEngineInstance().getAllReferenceEngine();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            ReferenceEngine currentBO  = null;            
	            Iterator<ReferenceEngine> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getReferenceEnginePrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ReferenceEngineTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate ReferenceEngine
     * 
     * @return    ReferenceEngine
     */    
    protected ReferenceEngine getNewBO()
    {
        ReferenceEngine newBO = new ReferenceEngine();

// AIB : \#defaultBOOutput() 
newBO.setName(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
newBO.setActive(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setPurpose(
    Purpose.getDefaultValue()
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ReferenceEnginePrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(ReferenceEngine.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}

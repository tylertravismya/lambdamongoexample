/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test ReferenceGiver class.
 *
 * @author    Dev Team
 */
public class ReferenceGiverTest
{

// constructors

    public ReferenceGiverTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a ReferenceGiver, through a ReferenceGiverTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on ReferenceGiverTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on ReferenceGiverTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new ReferenceGiver.
     *
     * @return    ReferenceGiver
     */
    public ReferenceGiver testCreate()
    throws Throwable
    {
        ReferenceGiver businessObject = null;

    	{
	        LOGGER.info( "ReferenceGiverTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a ReferenceGiver");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a ReferenceGiver" );
	
	        try 
	        {            
	            businessObject = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().createReferenceGiver( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (ReferenceGiverPrimaryKey)businessObject.getReferenceGiverPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a ReferenceGiver with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a ReferenceGiver.
     *
     * @return    ReferenceGiver  
     */
    public ReferenceGiver testRead()
    throws Throwable
    {
        LOGGER.info( "ReferenceGiverTest:testRead()" );
        LOGGER.info( "-- Reading a previously created ReferenceGiver" );

        ReferenceGiver businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read ReferenceGiver with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getReferenceGiver( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found ReferenceGiver " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a ReferenceGiver.
     *
     * @return    ReferenceGiver
     */
    public ReferenceGiver testUpdate()
    throws Throwable
    {

        LOGGER.info( "ReferenceGiverTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a ReferenceGiver." );

        StringBuilder msg = new StringBuilder( "Failed to update a ReferenceGiver : " );        
        ReferenceGiver businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created ReferenceGiver." );
            
            // for use later on...
            thePrimaryKey = (ReferenceGiverPrimaryKey)businessObject.getReferenceGiverPrimaryKey();
            
            ReferenceGiverBusinessDelegate proxy = ReferenceGiverBusinessDelegate.getReferenceGiverInstance();            
            businessObject = proxy.saveReferenceGiver( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved ReferenceGiver - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a ReferenceGiver.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "ReferenceGiverTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created ReferenceGiver." );
        
        try
        {
            ReferenceGiverBusinessDelegate.getReferenceGiverInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted ReferenceGiver with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete ReferenceGiver with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all ReferenceGivers.
     *
     * @return    Collection
     */
    public ArrayList<ReferenceGiver> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "ReferenceGiverTest:testGetAll() - Retrieving Collection of ReferenceGivers:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all ReferenceGiver : " );        
        ArrayList<ReferenceGiver> collection  = null;

        try
        {
            // call the static get method on the ReferenceGiverBusinessDelegate
            collection = ReferenceGiverBusinessDelegate.getReferenceGiverInstance().getAllReferenceGiver();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            ReferenceGiver currentBO  = null;            
	            Iterator<ReferenceGiver> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getReferenceGiverPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public ReferenceGiverTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate ReferenceGiver
     * 
     * @return    ReferenceGiver
     */    
    protected ReferenceGiver getNewBO()
    {
        ReferenceGiver newBO = new ReferenceGiver();

// AIB : \#defaultBOOutput() 
newBO.setDateLastUpdated(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setActive(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setDateTimeLastViewedExternally(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setMakeViewableToUser(
    new Boolean( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setDateLastSent(
    java.util.Calendar.getInstance().getTime()
 );
newBO.setNumDaysToExpire(
    new Integer( new String(org.apache.commons.lang3.RandomStringUtils.randomNumeric(3) )  )
 );
newBO.setStatus(
    ReferenceStatus.getDefaultValue()
 );
newBO.setType(
    Purpose.getDefaultValue()
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected ReferenceGiverPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(ReferenceGiver.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}

/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.test;

import java.io.*;
import java.util.*;
import java.util.logging.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertNotNull;

    import com.realmethods.primarykey.*;
    import com.realmethods.delegate.*;
    import com.realmethods.bo.*;
    
/**
 * Test Comment class.
 *
 * @author    Dev Team
 */
public class CommentTest
{

// constructors

    public CommentTest()
    {
    	LOGGER.setUseParentHandlers(false);	// only want to output to the provided LogHandler
    }

// test methods
    @Test
    /** 
     * Full Create-Read-Update-Delete of a Comment, through a CommentTest.
     */
    public void testCRUD()
    throws Throwable
   {        
        try
        {
        	LOGGER.info( "**********************************************************" );
            LOGGER.info( "Beginning full test on CommentTest..." );
            
            testCreate();            
            testRead();        
            testUpdate();
            testGetAll();                
            testDelete();
            
            LOGGER.info( "Successfully ran a full test on CommentTest..." );
            LOGGER.info( "**********************************************************" );
            LOGGER.info( "" );
        }
        catch( Throwable e )
        {
            throw e;
        }
        finally 
        {
        	if ( handler != null ) {
        		handler.flush();
        		LOGGER.removeHandler(handler);
        	}
        }
   }

    /** 
     * Tests creating a new Comment.
     *
     * @return    Comment
     */
    public Comment testCreate()
    throws Throwable
    {
        Comment businessObject = null;

    	{
	        LOGGER.info( "CommentTest:testCreate()" );
	        LOGGER.info( "-- Attempting to create a Comment");
	
	        StringBuilder msg = new StringBuilder( "-- Failed to create a Comment" );
	
	        try 
	        {            
	            businessObject = CommentBusinessDelegate.getCommentInstance().createComment( getNewBO() );
	            assertNotNull( businessObject, msg.toString() );
	
	            thePrimaryKey = (CommentPrimaryKey)businessObject.getCommentPrimaryKey();
	            assertNotNull( thePrimaryKey, msg.toString() + " Contains a null primary key" );
	
	            LOGGER.info( "-- Successfully created a Comment with primary key" + thePrimaryKey );
	        }
	        catch (Exception e) 
	        {
	            LOGGER.warning( unexpectedErrorMsg );
	            LOGGER.warning( msg.toString() + businessObject );
	            
	            throw e;
	        }
    	}
        return businessObject;
    }

    /** 
     * Tests reading a Comment.
     *
     * @return    Comment  
     */
    public Comment testRead()
    throws Throwable
    {
        LOGGER.info( "CommentTest:testRead()" );
        LOGGER.info( "-- Reading a previously created Comment" );

        Comment businessObject = null;
        StringBuilder msg = new StringBuilder( "-- Failed to read Comment with primary key" );
        msg.append( thePrimaryKey );

        try
        {
            businessObject = CommentBusinessDelegate.getCommentInstance().getComment( thePrimaryKey );
            
            assertNotNull( businessObject,msg.toString() );

            LOGGER.info( "-- Successfully found Comment " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests updating a Comment.
     *
     * @return    Comment
     */
    public Comment testUpdate()
    throws Throwable
    {

        LOGGER.info( "CommentTest:testUpdate()" );
        LOGGER.info( "-- Attempting to update a Comment." );

        StringBuilder msg = new StringBuilder( "Failed to update a Comment : " );        
        Comment businessObject = null;
    
        try
        {            
            businessObject = testCreate();
            
            assertNotNull( businessObject, msg.toString() );

            LOGGER.info( "-- Now updating the created Comment." );
            
            // for use later on...
            thePrimaryKey = (CommentPrimaryKey)businessObject.getCommentPrimaryKey();
            
            CommentBusinessDelegate proxy = CommentBusinessDelegate.getCommentInstance();            
            businessObject = proxy.saveComment( businessObject );   
            
            assertNotNull( businessObject, msg.toString()  );

            LOGGER.info( "-- Successfully saved Comment - " + businessObject.toString() );
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() + " : primarykey-" + thePrimaryKey + " : businessObject-" +  businessObject + " : " + e );
            
            throw e;
        }

        return( businessObject );
    }

    /** 
     * Tests deleting a Comment.
     */
    public void testDelete()
    throws Throwable
    {
        LOGGER.info( "CommentTest:testDelete()" );
        LOGGER.info( "-- Deleting a previously created Comment." );
        
        try
        {
            CommentBusinessDelegate.getCommentInstance().delete( thePrimaryKey );
            
            LOGGER.info( "-- Successfully deleted Comment with primary key " + thePrimaryKey );            
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( "-- Failed to delete Comment with primary key " + thePrimaryKey );
            
            throw e;
        }
    }

    /** 
     * Tests getting all Comments.
     *
     * @return    Collection
     */
    public ArrayList<Comment> testGetAll()
    throws Throwable
    {    
        LOGGER.info( "CommentTest:testGetAll() - Retrieving Collection of Comments:" );

        StringBuilder msg = new StringBuilder( "-- Failed to get all Comment : " );        
        ArrayList<Comment> collection  = null;

        try
        {
            // call the static get method on the CommentBusinessDelegate
            collection = CommentBusinessDelegate.getCommentInstance().getAllComment();

            if ( collection == null || collection.size() == 0 )
            {
                LOGGER.warning( unexpectedErrorMsg );
                LOGGER.warning( "-- " + msg.toString() + " Empty collection returned."  );
            }
            else
            {
	            // Now print out the values
	            Comment currentBO  = null;            
	            Iterator<Comment> iter = collection.iterator();
					
	            while( iter.hasNext() )
	            {
	                // Retrieve the businessObject   
	                currentBO = iter.next();
	                
	                assertNotNull( currentBO,"-- null value object in Collection." );
	                assertNotNull( currentBO.getCommentPrimaryKey(), "-- value object in Collection has a null primary key" );        
	
	                LOGGER.info( " - " + currentBO.toString() );
	            }
            }
        }
        catch ( Throwable e )
        {
            LOGGER.warning( unexpectedErrorMsg );
            LOGGER.warning( msg.toString() );
            
            throw e;
        }

        return( collection );
    }
    
    public CommentTest setHandler( Handler handler ) {
    	this.handler = handler;
    	LOGGER.addHandler(handler);	// assign so the LOGGER can only output results to the Handler
    	return this;
    }
    
    /** 
     * Returns a new populate Comment
     * 
     * @return    Comment
     */    
    protected Comment getNewBO()
    {
        Comment newBO = new Comment();

// AIB : \#defaultBOOutput() 
newBO.setCommentText(
    new String( org.apache.commons.lang3.RandomStringUtils.randomAlphabetic(10) )
 );
// ~AIB

        return( newBO );
    }
    
// attributes 

    protected CommentPrimaryKey thePrimaryKey      = null;
	protected Properties frameworkProperties 			= null;
	private final Logger LOGGER = Logger.getLogger(Comment.class.getName());
	private Handler handler = null;
	private String unexpectedErrorMsg = ":::::::::::::: Unexpected Error :::::::::::::::::";
}

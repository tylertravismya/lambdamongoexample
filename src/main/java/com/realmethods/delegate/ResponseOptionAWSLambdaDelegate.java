/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.delegate;

import java.util.*;
import java.io.IOException;

//import java.util.logging.Level;
//import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.*;

import io.swagger.annotations.*;

import com.amazonaws.services.lambda.runtime.Context;

    import com.realmethods.primarykey.*;
    import com.realmethods.dao.*;
    import com.realmethods.bo.*;
    
import com.realmethods.exception.CreationException;
import com.realmethods.exception.DeletionException;
import com.realmethods.exception.NotFoundException;
import com.realmethods.exception.SaveException;

/**
 * ResponseOption AWS Lambda Proxy delegate class.
 * <p>
 * This class implements the Business Delegate design pattern for the purpose of:
 * <ol>
 * <li>Reducing coupling between the business tier and a client of the business tier by hiding all business-tier implementation details</li>
 * <li>Improving the available of ResponseOption related services in the case of a ResponseOption business related service failing.</li>
 * <li>Exposes a simpler, uniform ResponseOption interface to the business tier, making it easy for clients to consume a simple Java object.</li>
 * <li>Hides the communication protocol that may be required to fulfill ResponseOption business related services.</li>
 * </ol>
 * <p>
 * @author Dev Team
 */
@Api(value = "ResponseOption", description = "RESTful API to interact with ResponseOption resources.")
@Path("/ResponseOption")
public class ResponseOptionAWSLambdaDelegate 
extends BaseAWSLambdaDelegate
{
//************************************************************************
// Public Methods
//************************************************************************
    /** 
     * Default Constructor 
     */
    public ResponseOptionAWSLambdaDelegate() {
	}


    /**
     * Creates the provided ResponseOption
     * @param		businessObject 	ResponseOption
	 * @param		context		Context	
     * @return     	ResponseOption
     * @exception   CreationException
     */
    @ApiOperation(value = "Creates a ResponseOption", notes = "Creates ResponseOption using the provided data" )
    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ResponseOption createResponseOption( 
    		@ApiParam(value = "ResponseOption entity to create", required = true) ResponseOption businessObject, 
    		Context context ) 
    	throws CreationException {
    	
		if ( businessObject == null )
        {
            String errMsg = "Null ResponseOption provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new CreationException( errMsg ); 
        }
      
        // return value once persisted
        ResponseOptionDAO dao  = getResponseOptionDAO();
        
        try
        {
            businessObject = dao.createResponseOption( businessObject );
        }
        catch (Exception exc)
        {
        	String errMsg = "ResponseOptionAWSLambdaDelegate:createResponseOption() - Unable to create ResponseOption" + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new CreationException( errMsg );
        }
        finally
        {
            releaseResponseOptionDAO( dao );            
        }        
         
        return( businessObject );
         
     }

    /**
     * Method to retrieve the ResponseOption via a supplied ResponseOptionPrimaryKey.
     * @param 	key
	 * @param	context		Context
     * @return 	ResponseOption
     * @exception NotFoundException - Thrown if processing any related problems
     */
    @ApiOperation(value = "Gets a ResponseOption", notes = "Gets the ResponseOption associated with the provided primary key", response = ResponseOption.class)
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ResponseOption getResponseOption( 
    		@ApiParam(value = "ResponseOption primary key", required = true) ResponseOptionPrimaryKey key, 
    		Context context  ) 
    	throws NotFoundException {
        
        ResponseOption businessObject  	= null;                
        ResponseOptionDAO dao 			= getResponseOptionDAO();
            
        try
        {
        	businessObject = dao.findResponseOption( key );
        }
        catch( Exception exc )
        {
            String errMsg = "Unable to locate ResponseOption with key " + key.toString() + " - " + getContextDetails(context) + exc;
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
            releaseResponseOptionDAO( dao );
        }        
        
        return businessObject;
    }
     
   /**
    * Saves the provided ResponseOption
    * @param		businessObject		ResponseOption
	* @param		context		Context	
    * @return       what was just saved
    * @exception    SaveException
    */
    @ApiOperation(value = "Saves a ResponseOption", notes = "Saves ResponseOption using the provided data" )
    @PUT
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    public static ResponseOption saveResponseOption( 
    		@ApiParam(value = "ResponseOption entity to save", required = true) ResponseOption businessObject, Context context  ) 
    	throws SaveException {

    	if ( businessObject == null )
        {
            String errMsg = "Null ResponseOption provided but not allowed " + getContextDetails(context);
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg ); 
        }
    	
        // --------------------------------
        // If the businessObject has a key, find it and apply the businessObject
        // --------------------------------
        ResponseOptionPrimaryKey key = businessObject.getResponseOptionPrimaryKey();
                    
        if ( key != null )
        {
            ResponseOptionDAO dao = getResponseOptionDAO();

            try
            {                    
                businessObject = (ResponseOption)dao.saveResponseOption( businessObject );
            }
            catch (Exception exc)
            {
                String errMsg = "Unable to save ResponseOption" + getContextDetails(context) + exc;
                context.getLogger().log( errMsg );
                throw new SaveException( errMsg );
            }
            finally
            {
            	releaseResponseOptionDAO( dao );
            }
        }
        else
        {
            String errMsg = "Unable to create ResponseOption due to it having a null ResponseOptionPrimaryKey."; 
            context.getLogger().log( errMsg );
            throw new SaveException( errMsg );
        }
		        
        return( businessObject );
        
    }
     

	/**
     * Method to retrieve a collection of all ResponseOptions
     * @param		context		Context
     * @return 	ArrayList<ResponseOption> 
     */
    @ApiOperation(value = "Get all ResponseOption", notes = "Get all ResponseOption from storage", responseContainer = "ArrayList", response = ResponseOption.class)
    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)    
    public static ArrayList<ResponseOption> getAllResponseOption( Context context ) 
    	throws NotFoundException {

        ArrayList<ResponseOption> array	= null;
        ResponseOptionDAO dao 			= getResponseOptionDAO();
        
        try
        {
            array = dao.findAllResponseOption();
        }
        catch( Exception exc )
        {
            String errMsg = "failed to getAllResponseOption - " + getContextDetails(context) + exc.getMessage();
            context.getLogger().log( errMsg );
            throw new NotFoundException( errMsg );
        }
        finally
        {
        	releaseResponseOptionDAO( dao );
        }        
        
        return array;
    }
           
     
    /**
     * Deletes the associated business object using the provided primary key.
     * @param		key 	ResponseOptionPrimaryKey
     * @param		context		Context    
     * @exception 	DeletionException
     */
    @ApiOperation(value = "Deletes a ResponseOption", notes = "Deletes the ResponseOption associated with the provided primary key", response = ResponseOption.class)
    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)    
	public static void deleteResponseOption( 
			@ApiParam(value = "ResponseOption primary key", required = true) ResponseOptionPrimaryKey key, 
			Context context  ) 
    	throws DeletionException {    	

    	if ( key == null )
        {
            String errMsg = "Null key provided but not allowed " + getContextDetails(context) ;
            context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }

        ResponseOptionDAO dao  = getResponseOptionDAO();

		boolean deleted = false;
		
        try
        {                    
            deleted = dao.deleteResponseOption( key );
        }
        catch (Exception exc)
        {
        	String errMsg = "Unable to delete ResponseOption using key = "  + key + ". " + getContextDetails(context) + exc;
        	context.getLogger().log( errMsg );
            throw new DeletionException( errMsg );
        }
        finally
        {
            releaseResponseOptionDAO( dao );
        }
         		
        return;
     }

// role related methods

 
 	
    /**
     * Returns the ResponseOption specific DAO.
     *
     * @return      ResponseOption DAO
     */
    public static ResponseOptionDAO getResponseOptionDAO()
    {
        return( new com.realmethods.dao.ResponseOptionDAO() ); 
    }

    /**
     * Release the ResponseOptionDAO back to the FrameworkDAOFactory
     */
    public static void releaseResponseOptionDAO( com.realmethods.dao.ResponseOptionDAO dao )
    {
        dao = null;
    }
    
//************************************************************************
// Attributes
//************************************************************************

//    private static final Logger LOGGER = Logger.getLogger(ResponseOptionAWSLambdaDelegate.class.getName());
}


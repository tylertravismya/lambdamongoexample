/*******************************************************************************
  Turnstone Biologics Confidential
  
  2018 Turnstone Biologics
  All Rights Reserved.
  
  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.
   
  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.realmethods.dao;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.mongodb.morphia.Datastore;

import org.bson.Document;
import com.mongodb.client.MongoCollection;

import com.realmethods.exception.*;

    import com.realmethods.primarykey.*;
    import com.realmethods.bo.*;
    
/** 
 * Implements the MongoDB NoSQL persistence processing for business entity ReferenceGiver
 * using the Morphia Java Datastore.
 *
 * @author Dev Team
 */
 public class ReferenceGiverDAO extends BaseDAO
{
    /**
     * default constructor
     */
    public ReferenceGiverDAO()
    {
    }

	
//*****************************************************
// CRUD methods
//*****************************************************

    /**
     * Retrieves a ReferenceGiver from the persistent store, using the provided primary key. 
     * If no match is found, a null ReferenceGiver is returned.
     * <p>
     * @param       pk
     * @return      ReferenceGiver
     * @exception   ProcessingException
     */
    public ReferenceGiver findReferenceGiver( ReferenceGiverPrimaryKey pk ) 
    throws ProcessingException
    {
    	ReferenceGiver businessObject = null;
    	
        if (pk == null)
        {
            throw new ProcessingException("ReferenceGiverDAO.findReferenceGiver cannot have a null primary key argument");
        }
    
		Datastore dataStore = getDatastore();
        try
        {
        	businessObject = dataStore.createQuery( ReferenceGiver.class )
        	       						.field( "referenceGiverId" )
        	       						.equal( (Long)pk.getFirstKey() )
        	       						.get();
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGiverDAO.findReferenceGiver failed for primary key " + pk + " - " + exc );		
		}		
		finally
		{
		}		    
		
        return( businessObject );
    }
    
    /**
     * Inserts a new ReferenceGiver into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceGiver
     * @exception   ProcessingException
     */
    public ReferenceGiver createReferenceGiver( ReferenceGiver businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		// let's assign out internal unique Id
    		Long id = getNextSequence( "referenceGiverId" ) ;
    		businessObject.setReferenceGiverId( id );
    		
    		dataStore.save( businessObject );
    	    LOGGER.info( "---- ReferenceGiverDAO.createReferenceGiver created a bo is " + businessObject );
    	}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGiverDAO.createReferenceGiver - " + exc.getMessage() );
		}		
		finally
		{
		}		
		
        // return the businessObject
        return(  businessObject );
    }
    
    /**
     * Stores the provided ReferenceGiver to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceGiver	stored entity
     * @exception   ProcessingException 
     */
    public ReferenceGiver saveReferenceGiver( ReferenceGiver businessObject )
    throws ProcessingException
    {
    	Datastore dataStore = getDatastore();    	
    	
    	try
    	{
    		dataStore.save( businessObject );
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			throw new ProcessingException( "ReferenceGiverDAO:saveReferenceGiver - " + exc.getMessage() );
		}		
		finally
		{
		}		    

    	return( businessObject );
    }
    
    /**
    * Removes a ReferenceGiver from the persistent store.
    *
    * @param        pk		identity of object to remove
    * @exception    ProcessingException
    */
    public boolean deleteReferenceGiver( ReferenceGiverPrimaryKey pk ) 
    throws ProcessingException 
    {
    	Datastore dataStore = getDatastore();
    	
    	try
    	{
    		dataStore.delete( findReferenceGiver( pk ) );
		}
		catch( Throwable exc )
		{
			LOGGER.severe("ReferenceGiverDAO:delete() - failed " + exc.getMessage() );
			
			exc.printStackTrace();			
			throw new ProcessingException( "ReferenceGiverDAO.deleteReferenceGiver failed - " + exc );					
		}		
		finally
		{
		}
    	
    	return( true );
    }

    /**
     * returns a Collection of all ReferenceGivers
     * @return		ArrayList<ReferenceGiver>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceGiver> findAllReferenceGiver()
    throws ProcessingException
    {
		final ArrayList<ReferenceGiver> list 	= new ArrayList<ReferenceGiver>();
		Datastore dataStore 				= getDatastore();
		
		try
		{
			list.addAll( dataStore.createQuery( ReferenceGiver.class ).asList() );
			
		}
		catch( Throwable exc )
		{
			exc.printStackTrace();
			LOGGER.warning( "ReferenceGiverDAO.findAll() errors - " + exc.getMessage() );
			throw new ProcessingException( "ReferenceGiverDAO.findAllReferenceGiver failed - " + exc );		
		}		
		finally
		{
		}
		
		if ( list.size() <= 0 )
		{
			LOGGER.info( "ReferenceGiverDAO:findAllReferenceGivers() - List is empty.");
		}
        
		return( list );		        
    }		

// abstracts and overloads from BaseDAO

	@Override
	protected String getCollectionName()
	{
		return( "com.realmethods.ReferenceGiver" );
	}

	@Override
	protected MongoCollection<Document> createCounterCollection( MongoCollection<Document> collection )
	{
		if ( collection != null ) 
		{
		    Document document = new Document();
		    Long initialValue = new Long(0);
		    
		    document.append( MONGO_ID_FIELD_NAME, "referenceGiverId" );
		    document.append( MONGO_SEQ_FIELD_NAME, initialValue );
    	    collection.insertOne(document);
		}
		
		return( collection );
	}


//*****************************************************
// Attributes
//*****************************************************
	private static final Logger LOGGER = Logger.getLogger(ReferenceGiver.class.getName());
}

# Specify the provider and access details
provider "aws" {
  region     = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
  version = "~> 2.0"
}

locals {
  public_key_filename  = "${path.root}/keys/id_rsa.pub"
  private_key_filename = "${path.root}/keys/id_rsa"
}

# Generate an RSA key to be used
resource "tls_private_key" "generated" {
  algorithm = "RSA"
}

# Generate the local SSH Key pair in the directory specified
resource "local_file" "public_key_openssh" {
  content  = "${tls_private_key.generated.public_key_openssh}"
  filename = "${local.public_key_filename}"
}
resource "local_file" "private_key_pem" {
  content  = "${tls_private_key.generated.private_key_pem}"
  filename = "${local.private_key_filename}"
}

resource "aws_key_pair" "generated" {
  key_name   = "pjsk-sshtest-${uuid()}"
  public_key = "${tls_private_key.generated.public_key_openssh}"

  lifecycle {
    ignore_changes = ["key_name"]
  }
}

 
# Default vpc
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
}

# Our default security group to for the database
resource "aws_security_group" "mongo" {
  description = "security group created from terraform"
  vpc_id      = "vpc-c422e2a0" # only valid for us-east-1

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # mongodb access from anywhere
  ingress {
    from_port   = 27017
    to_port     = 27017
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "mongodb" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
    private_key = "${tls_private_key.generated.private_key_pem}"
  }

  instance_type = "t2.micro"
  
  tags = { Name = "mongodb instance" } 

  # standard realmethods community image with mongo started on the default port 
  ami = "ami-0e2a167cf2e0ce6c0"

  # The name of the SSH keypair you've created and downloaded
  # from the AWS console.
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:
  #
  # key_name = ""
  key_name = "${aws_key_pair.generated.key_name}"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.mongo.id}"]
  
  # To ensure ssh access works
    provisioner "remote-exec" {
    inline = [
      "sudo ls",
    ]
  }
}

resource "aws_lambda_function" "getReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::getComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::addComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::assignComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerComments" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferrerComments"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::deleteComments"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::createReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::getReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::getAllReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::saveReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerAWSLambdaDelegate::deleteReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiverQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiverUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverReferrer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiverReferrer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteReferrer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverLastQuestionAnswered" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiverLastQuestionAnswered"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteLastQuestionAnswered"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::addAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::assignAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiverAnswers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiverAnswers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteAnswers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::createReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::getAllReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::saveReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGiver" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGiver"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGiverAWSLambdaDelegate::deleteReferenceGiver"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::addReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::assignReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerGroupReferences" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferrerGroupReferences"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::deleteReferences"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::createReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::getAllReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::saveReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferrerGroupAWSLambdaDelegate::deleteReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::saveMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceEngineMainQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceEngineMainQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::deleteMainQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::createReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::getAllReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::saveReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceEngine" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceEngine"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceEngineAWSLambdaDelegate::deleteReferenceEngine"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::addResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::assignResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionResponses" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteQuestionResponses"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::deleteResponses"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::createQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::getAllQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::saveQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionAWSLambdaDelegate::deleteQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::createResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::getResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::getAllResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::saveResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteResponseOption" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteResponseOption"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ResponseOptionAWSLambdaDelegate::deleteResponseOption"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::addQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::assignQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroupQuestions" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteQuestionGroupQuestions"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestions"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::addQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::assignQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroupQuestionGroups" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteQuestionGroupQuestionGroups"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestionGroups"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::createQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::getAllQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::saveQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteQuestionGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteQuestionGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.QuestionGroupAWSLambdaDelegate::deleteQuestionGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::addUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::assignUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdminUsers" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAdminUsers"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteUsers"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "addAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_addAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::addReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "assignAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_assignAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::assignReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdminReferenceEngines" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAdminReferenceEngines"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteReferenceEngines"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::createAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::getAllAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::saveAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAdmin" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAdmin"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AdminAWSLambdaDelegate::deleteAdmin"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::saveUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteActivityUser" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteActivityUser"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::deleteUser"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::createActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::getAllActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::saveActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteActivity" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteActivity"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ActivityAWSLambdaDelegate::deleteActivity"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::saveSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteCommentSource" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteCommentSource"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::deleteSource"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::createComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::getAllComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::saveComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteComment" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteComment"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.CommentAWSLambdaDelegate::deleteComment"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswerQuestion" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAnswerQuestion"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteQuestion"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswerResponse" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAnswerResponse"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteResponse"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::createAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::getAllAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::saveAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteAnswer" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteAnswer"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.AnswerAWSLambdaDelegate::deleteAnswer"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLinkReferrerGroup" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGroupLinkReferrerGroup"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteReferrerGroup"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLinkLinkProvider" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGroupLinkLinkProvider"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteLinkProvider"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "createReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_createReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::createReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "getAllReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_getAllReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::getAllReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "saveReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_saveReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::saveReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}

resource "aws_lambda_function" "deleteReferenceGroupLink" {
  filename         = "/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar"
  function_name    = "lambdaFunc_deleteReferenceGroupLink"
  role             = "arn:aws:iam::110777515443:role/service-role/myRoleName"
  handler          = "com.realmethods.delegate.ReferenceGroupLinkAWSLambdaDelegate::deleteReferenceGroupLink"
  source_code_hash = "${filebase64sha256("/home/circleci/gitRoot/target/lambdamongodemo-0.0.1.jar")}"
  runtime          = "java8"
  memory_size      = "512"
  timeout          = "30"
  publish          = "true"
  environment {
    variables = {
      mongoDbServerAddresses = "${aws_instance.mongodb.public_ip}:27017"
    }
  }
  vpc_config {
     subnet_ids = [""]
     security_group_ids = [""]
  }  
}




